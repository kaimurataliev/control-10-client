import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.css';

const Header = () => {
    return (
        <header className="Toolbar">
            <nav>
                <NavLink className="link" to="/new-post">Add new post</NavLink>
                <NavLink className="link" to="/">All news</NavLink>
            </nav>
        </header>
    )
};

export default Header;