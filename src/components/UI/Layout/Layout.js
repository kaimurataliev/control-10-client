import React, { Fragment } from 'react';
import './Layout.css';
import Header from '../Header/Header';

const Layout = props => {
    return (
        <Fragment>
            <Header />
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    )
};

export default Layout;
