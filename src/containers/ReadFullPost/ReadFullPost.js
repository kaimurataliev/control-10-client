import React, {Component} from 'react';
import { connect } from 'react-redux';
import {addComment, deleteComment, fetchComments, fetchNewsById} from "../../store/actions";
import './ReadFullPost.css';

class ReadFullPost extends Component {

    state = {
        news_id: this.props.match.params.id,
        author: '',
        comment: ''
    };

    componentDidMount() {
        this.props.fetchNewsById(this.props.match.params.id).then(() => {
            this.props.fetchComments(this.props.match.params.id)
        });
    }

    getAuthor = (event) => {
        this.setState({author: event.target.value})
    };

    getComment = (event) => {
        this.setState({comment: event.target.value})
    };

    onSendComment = (event) => {
        event.preventDefault();
        this.props.addComment(this.state)
    };

    onDeleteHandler = (id) => {
        this.props.deleteComment(id).then(() => {
            this.props.fetchComments(this.props.match.params.id)
        })
    };

    render() {
        const comments = Object.keys(this.props.comments);

        return (
            <div className="full-container">

                <div className="post-info">
                    <h3>Info about this post</h3>
                    <p>Date of publication: {this.props.oneInfo.date}</p>
                    <p>detailed information: {this.props.oneInfo.description}</p>
                </div>

                <div className="comments">
                    <h3>Comments</h3>
                    {comments.map((comment, index) => {
                        return (
                            <div className="comment" key={index}>
                                <h5>Author of comment: {this.props.comments[comment].author}</h5>
                                <p>comment: {this.props.comments[comment].comment}</p>
                                <button className="delete" onClick={() => this.onDeleteHandler(this.props.comments[comment].id)}>Delete</button>
                            </div>
                        )
                    })}
                </div>

                <form className="comment-form" onSubmit={this.onSendComment}>
                    <h3>Add new comment</h3>
                    <label htmlFor="author">Enter author name</label>
                    <input type="text"
                           name="author"
                           onChange={this.getAuthor}
                    />

                    <label htmlFor="comment">Enter comment</label>
                    <input type="text"
                           name='comment'
                           onChange={this.getComment}
                    />

                    <button type="submit">Add comment</button>
                </form>
                </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        oneInfo: state.oneInfo,
        comments: state.comments
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addComment: (data) => dispatch(addComment(data)),
        fetchNewsById: (id) => dispatch(fetchNewsById(id)),
        fetchComments: (id) => dispatch(fetchComments(id)),
        deleteComment: (id) => dispatch(deleteComment(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ReadFullPost);