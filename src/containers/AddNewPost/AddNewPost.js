import React, { Component } from 'react';
import { connect } from 'react-redux';
import {sendPost} from "../../store/actions";
import './AddNewPost.css';

class AddNewPost extends Component {

    state = {
        title: '',
        description: '',
        image: ''
    };


    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        });
    };

    getTitle = (event) => {
        this.setState({title: event.target.value});
        // this.props.clearError();
    };


    getDescription = (event) => {
        this.setState({description: event.target.value});
    };

    sendHandler = (event) => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach((key) => {
            formData.append(key, this.state[key])
        });

        this.props.sendPost(formData);
        this.setState({title: '', image: '', description: ''})

    };

    render() {
        return (
            <div className="form-container">
                <form className="form" required onSubmit={this.sendHandler}>
                    <h1>Add new post</h1>
                    <label htmlFor="title">Enter title</label>
                    <input type="text"
                           required
                           name="title"
                           placeholder="Title"
                           onChange={this.getTitle}
                    />
                    <label htmlFor="description">Enter description</label>
                    <textarea
                            type="text"
                            required
                            cols="30" rows="10"
                            name="description"
                            placeholder="description"
                            onChange={this.getDescription}
                    />
                    <label htmlFor="image">Choose image</label>
                    <input type="file"
                           name="image"
                           onChange={this.fileChangeHandler}
                    />

                    <button type="submit">Save</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {

    }
};

const mapDispatchToProps = dispatch => {
    return {
        sendPost: (post) => dispatch(sendPost(post))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPost);