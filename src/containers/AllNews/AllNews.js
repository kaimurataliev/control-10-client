import React, { Component } from 'react';
import { connect } from 'react-redux';
import {deletePost, fetchNews, fetchNewsById} from "../../store/actions";
import {NavLink} from 'react-router-dom';
import './AllNews.css';

class AllNews extends Component {

    componentDidMount() {
        this.props.fetchNews();
    }

    onDelete = (event, id) => {
        event.preventDefault();
        this.props.deletePost(id).then(() => {
            this.props.fetchNews();
        });
    };

    goToRoute = (event, id) => {
        event.preventDefault();
        this.props.history.push(`/about/${id}`)
    };


    render() {
        return (
            <div className="container">
                {this.props.news.map((post, index) => {
                    return (
                        <div key={index} className="post">
                            <h4>{post.title}</h4>
                            {post.image ? <img src={'http://localhost:8000/images/' + post.image} alt="photo"/> : null }
                            <button onClick={(event) => this.onDelete(event, post.id)}>Delete post</button>

                            <NavLink className='read-full' to="/about">
                                <button onClick={(event) => this.goToRoute(event, post.id)}>Read full post</button>
                            </NavLink>
                            <span>Date: {post.date}</span>
                        </div>
                    )
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        news: state.news
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchNews: () => dispatch(fetchNews()),
        deletePost: (id) => dispatch(deletePost(id)),
        fetchNewsById: (id) => dispatch(fetchNewsById(id))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AllNews);