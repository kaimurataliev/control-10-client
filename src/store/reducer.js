import * as actions from './actions';

const initialState = {
    news: [],
    oneInfo: {},
    comments: {}
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_NEWS_SUCCESS:
            return {...state, news: action.data};

        case actions.FETCH_NEWS_ID_SUCCESS:
            return {...state, oneInfo: action.data};

        case actions.FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.data};

        default:
            return state;
    }
};

export default reducer;