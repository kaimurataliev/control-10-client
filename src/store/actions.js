import axios from '../axios-news';

export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_ID_SUCCESS = 'FETCH_NEWS_ID_SUCCESS';
export const FETCH_COMMENTS_SUCCESS = 'FETCH_COMMENTS_SUCCESS';

export const fetchCommentsSuccess = (data) => {
    return {type: FETCH_COMMENTS_SUCCESS, data}
};

export const deleteComment = (id) => {
    return (dispatch) => {
        return axios.delete(`/comments/${id}`);
    }
};

export const fetchComments = (id) => {
    return (dispatch) => {
        return axios.get(`/comments?news_id=${id}`).then(response => {
            dispatch(fetchCommentsSuccess(response.data));
        })
    }
};

export const fetchNewsIdSuccess = (data) => {
    return {type: FETCH_NEWS_ID_SUCCESS, data}
};

export const fetchNewsById = (id) => {
    return (dispatch) => {
        return axios.get(`/news/${id}`).then(response => {
            dispatch(fetchNewsIdSuccess(response.data))
        })
    }
};

export const addComment = (data) => {
    return (dispatch) => {
        return axios.post('/comments', data);
    }
};

export const sendPost = (post) => {
    return (dispatch) => {
        return axios.post('/news', post)
    }
};

export const fetchNewsSuccess = (data) => {
    return {type: FETCH_NEWS_SUCCESS, data}
};

export const deletePost = (id) => {
    return (dispatch) => {
        return axios.delete(`/news/${id}`);
    }
};

export const fetchNews = () => {
    return (dispatch) => {
        return axios.get('/news').then(response => {
            dispatch(fetchNewsSuccess(response.data))
        })
    }
};