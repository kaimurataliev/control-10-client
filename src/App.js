import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import Layout from './components/UI/Layout/Layout';
import AllNews from './containers/AllNews/AllNews';
import AddNewPost from './containers/AddNewPost/AddNewPost';
import ReadFullPost from './containers/ReadFullPost/ReadFullPost';

import './App.css';

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/" exact component={AllNews}/>
                    <Route path="/new-post" component={AddNewPost}/>
                    <Route path="/about/:id" component={ReadFullPost}/>
                </Switch>
            </Layout>
        );
    }
}

export default App;
